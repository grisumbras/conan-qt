from conans import (
    ConanFile,
    tools,
)
from conans.errors import ConanInvalidConfiguration
import configparser
import os
import shutil


def getsubmodules():
    config = configparser.ConfigParser()
    config.read("qtmodules.conf")
    res = {}
    assert config.sections()
    for s in config.sections():
        section = str(s)
        assert section.startswith("submodule ")
        assert section.count('"') == 2
        modulename = section[section.find('"') + 1: section.rfind('"')]
        status = str(config.get(section, "status"))
        if status != "obsolete" and status != "ignore":
            res[modulename] = {
                "branch": str(config.get(section, "branch")),
                "status": status,
                "path": str(config.get(section, "path")),
            }
            if config.has_option(section, "depends"):
                res[modulename]["depends"] = [str(i) for i in config.get(section, "depends").split()]
            else:
                res[modulename]["depends"] = []
    return res


class QtConan(ConanFile):
    _submodules = getsubmodules()

    name = "Qt"
    version = "5.7.1"
    description = "Qt is a cross-platform C++ application framework"
    url = "https://gitlab.com/grisumbras/conan-qt"
    homepage = "https://www.qt.io"
    license = "http://doc.qt.io/qt-5/lgpl.html"
    exports = "qtmodules.conf",
    settings = "os", "arch", "compiler", "build_type",

    options = dict(
        {
            "shared": [True, False],
            "commercial": [True, False],
            "opengl": ["no", "es2", "desktop", "dynamic"],
            "openssl": [True, False],
            "GUI": [True, False],
            "widgets": [True, False],
            "config": "ANY",
        },
        **{module: [True,False] for module in _submodules},
    )
    no_copy_source = True
    default_options = dict(
        {
            "shared": True,
            "commercial": False,
            "opengl": "desktop",
            "openssl": False,
            "GUI": True,
            "widgets": True,
            "config": None,
        },
        **{module: False for module in _submodules},
    )
    short_paths = True
    build_policy = "missing"

    def build_requirements(self):
        if tools.os_info.is_windows and self.settings.compiler == "Visual Studio":
            self.build_requires("jom_installer/1.1.2@bincrafters/stable")

    def configure(self):
        if self.options.openssl:
            self.requires("OpenSSL/1.0.2o@conan/stable")
            self.options["OpenSSL"].no_zlib = True

        if self.options.widgets:
            self.options.GUI = True
        if not self.options.GUI:
            self.options.opengl = "no"
        if self.settings.os == "Android" and self.options.opengl == "desktop":
            raise ConanInvalidConfiguration(
                "OpenGL desktop is not supported on Android. Consider using OpenGL es2"
            )

        assert QtConan.version == QtConan._submodules["qtbase"]["branch"]

        def _enablemodule(mod):
            setattr(self.options, mod, True)
            for req in QtConan._submodules[mod]["depends"]:
                _enablemodule(req)

        self.options.qtbase = True
        for module in QtConan._submodules:
            if getattr(self.options, module):
                _enablemodule(module)

    def source(self):
        basename = "qt-everywhere-opensource-src-" + self.version

        file = basename + (".zip" if tools.os_info.is_windows else ".tar.xz")
        url = (
            "http://download.qt.io/archive/qt/{0}/{1}/single/{2}"
            .format(self.version[:self.version.rfind(".")], self.version, file)
        )
        tools.get(url)

        if os.path.exists("qt5"):
            shutil.rmtree("qt5")
        shutil.move(basename, "qt5")

    def build(self):
        commands = []
        if self.should_configure:
            commands.append(self._configure())
        if self.should_build:
            commands.append(self._make())
        self._build_steps(*commands)

    def package(self):
        self._build_steps(self._make() + " install", cwd=self.build_folder)
        self.copy("bin/qt.conf", src="qtbase")

    def package_info(self):
        if self.settings.os == "Windows":
            self.env_info.path.append(os.path.join(self.package_folder, "bin"))
        self.env_info.CMAKE_PREFIX_PATH.append(self.package_folder)

    def _configure(self):
        args = [
            "-confirm-license",
            "-silent",
            "-nomake examples",
            "-nomake tests",
            "-qt-zlib",
            "-prefix %s" % self.package_folder,
        ]

        if self.options.commercial:
            args.append("-commercial")
        else:
            args.append("-opensource")

        if not self.options.GUI:
            args.append("-no-gui")

        if not self.options.widgets:
            args.append("-no-widgets")

        if not self.options.shared:
            args.insert(0, "-static")
            if self.settings.os == "Windows":
                if self.settings.compiler.runtime == "MT" or self.settings.compiler.runtime == "MTd":
                    args.append("-static-runtime")
        else:
            args.insert(0, "-shared")

        if self.settings.build_type == "Debug":
            args.append("-debug")
        elif self.settings.build_type == "RelWithDebInfo":
            args += ["-release", "-force-debug-info"]
        elif self.settings.build_type == "MinSizeRel":
            args += ["-release", "-optimize-size"]
        else:
            args.append("-release")

        for module in self._submodules:
            path = os.path.join(self.source_folder, "qt5", self._submodules[module]["path"])
            if not getattr(self.options, module) and os.path.isdir(path):
                args.append("-skip " + module)

        #openGL
        if self.options.opengl == "no":
            args.append("-no-opengl")
        elif self.options.opengl == "es2":
            args.append("-opengl es2")
        elif self.options.opengl == "desktop":
            args.append("-opengl desktop")
        if self.settings.os == "Windows":
            if self.options.opengl == "dynamic":
                args.append("-opengl dynamic")

        #openSSL
        if not self.options.openssl:
            args.append("-no-openssl")
        else:
            if self.options["OpenSSL"].shared:
                args.append("-openssl-linked")
            else:
                args.append("-openssl")
            args += ["-I %s" % i for i in self.deps_cpp_info["OpenSSL"].include_paths]

        if self.settings.os == "Linux":
            if self.options.GUI:
                args.append("-qt-xcb")
        elif self.settings.os == "Macos":
            args.append("-no-framework")
        elif self.settings.os == "Android":
            args.append("-android-ndk-platform android-%s" % self.settings.os.api_level)
            android_arch = {
                "armv6": "armeabi",
                "armv7": "armeabi-v7a",
                "armv8": "arm64-v8a",
                "x86": "x86",
                "x86_64": "x86_64",
                "mips": "mips",
                "mips64": "mips64"
            }.get(str(self.settings.arch))
            args.append("-android-arch %s" % android_arch)
            # args.append("-android-toolchain-version %s" % self.settings.compiler.version)

        xplatform_val = self._xplatform()
        if xplatform_val:
            args.append("-xplatform %s" % xplatform_val)
        else:
            self.output.warn("host not supported: %s %s %s %s" %
                             (self.settings.os, self.settings.compiler,
                              self.settings.compiler.version, self.settings.arch))

        if tools.cross_building(self.settings):
            chost = os.getenv("CHOST")
            if chost:
                args.append("-device-option CROSS_COMPILE=%s-" % chost)

        cc = tools.get_env("CC")
        if cc:
            args += ["-device-option QMAKE_CC=" + cc,
                     "-device-option QMAKE_LINK_C=" + cc,
                     "-device-option QMAKE_LINK_C_SHLIB=" + cc]

        cxx = tools.get_env("CXX")
        if cxx:
            args += ["-device-option QMAKE_CXX=" + cxx,
                     "-device-option QMAKE_LINK=" + cxx,
                     "-device-option QMAKE_LINK_SHLIB=" + cxx]

        if self.options.config:
            args.append(str(self.options.config))

        return "%s/qt5/configure %s" % (self.source_folder, " ".join(args))

    def _xplatform(self):
        xos, xcompiler= self._xos(), self._xcompiler()
        if (xos or xcompiler) is None:
            return

        xarch, xlibcxx, xis = self._xarch(), self._xlibcxx(), self._xinstruction_set()
        components = [
            x for x in (xos, xarch, xcompiler, xlibcxx, xis) if x is not None
        ]
        return "-".join(components)

    def _xos(self):
        return {
            "Linux": "linux",
            "Macos": "macx",
            "iOS": "macx-ios",
            "watchOS": "macx-watchos",
            "tvOS": "macx-tvos",
            "Android": "android",
            "Windows": "win32",
            "WindowsStore": "winrt",
            "FreeBSD": "freebsd",
            "SunOS": "solaris",
        }.get(str(self.settings.os))

    def _xcompiler(self):
        return {
            "gcc": "g++",
            "clang": "clang",
            "apple-clang": "clang",
            "Visual Studio": self._xmsvc(),
            "sun-cc": self._xsuncc(),
        }.get(str(self.settings.compiler))

    def _xmsvc(self):
        name = "msvc"
        if self.settings.os == "WindowsStore":
            if self.settings.compiler.version == "14":
                name += "2015"
            elif self.settings.compiler.version == "15":
                name += "2017"
                return name

    def _xsuncc(self):
        if self.settings.arch == "sparcv9":
            return "cc64"
        else:
            return "cc"

    def _xlibcxx(self):
        if (self.settings.os == "Linux"
            and self.settings.compiler == "clang"
            and self.settings.compiler.libcxx == "libc++"
        ):
            return "libc++"

        elif (self.settings.os == "SunOS"
            and self.settings.compiler == "sun-cc"
            and self.settings.compiler.libcxx == "libstlport"
        ):
            return "stlport"

    def _xarch(self):
        if (self.settings.os == "Linux"
            and self.settings.compiler == "gcc"
        ):
            return {
                "armv6": "arm-gnueabi",
                "armv7": "arm-gnueabi",
                "armv8": "aarch64-gnu",
            }.get(str(self.settings.arch))

        elif (self.settings.os == "WindowsStore"
              and self.settings.compiler == "Visual Studio"
        ):
            return {
                "armv7": "arm",
                "x86": "x86",
                "x86_64": "x64"
            }.get(str(self.settings.arch))

    def _xinstruction_set(self):
        if (self.settings.os == "Linux"
            and self.settings.arch == "x86"
            and self.settings.compiler in ("gcc", "clang")
        ):
            return "32"

        elif (self.settings.os == "SunOS"
              and self.settings.compiler == "gcc"
              and self.settings.arch == "sparcv9"
        ):
            return "64"

    def _make(self):
        if tools.os_info.is_windows:
            if self.settings.compiler == "Visual Studio":
                return "jom"
            else:
                return "mingw32-make"
        else:
            return "make"


    def _build_steps(self, *commands, **kwargs):
        env = {"MAKEFLAGS": "j%d" % tools.cpu_count()}
        if self.options.openssl:
            libs = self.deps_cpp_info["OpenSSL"].libs
            lib_paths = self.deps_cpp_info["OpenSSL"].lib_paths
            env["OPENSSL_LIBS"] = " ".join(["-L" + i for i in lib_paths] + ["-l" + i for i in libs])

        with self._build_context():
            with tools.environment_append(env):
                for command in commands:
                    self.run(command, **kwargs)

    def _build_context(self):
        if tools.os_info.is_windows:
            if self.settings.compiler == "Visual Studio":
                return tools.vcvars(self.settings)
            else:
                # Workaround for configure using clang first if in the path
                new_path = [
                    item
                    for item in tools.get_env("PATH", "").split(";")
                    if item != "C:\\Program Files\\LLVM\\bin"
                ]
                # end workaround
                return tools.environment_append({"PATH": ";".join(new_path)})
        else:
            return empty_context()


class empty_context(object):
    def __enter__(self):
        pass
    def __exit__(self, exc_type, exc_value, trcbk):
        pass
